function getColorByLevel(level){
    flLevel=parseFloat(level);
    if(flLevel<=20){
      return 'blue';
    }
    if(flLevel>20 && flLevel<=40){
      return 'lightgreen';
    }
    if(flLevel>40 && flLevel<=60){
      return 'beige';
    }
    if(flLevel>60 && flLevel<=80){
      return 'orange';
    }
    if(flLevel>80){
      return 'red';
    }
}

function setTable(item){
  tabla="<table><thead><tr><th>Propiedad</th><th>Valor</th></tr></thead><tbody>";
  tabla=tabla + "<tr><td>Medida (dB)</td><td>"+item.measurement.toString()+"</td></tr>";
  if(item.measurementDate!=null){
    tabla=tabla + "<tr><td>Fecha captura</td><td>"+item.measurementDate.toString().substring(0, 10);+"</td></tr>";
  }
  tabla=tabla + "<tr><td>Dispositivo</td><td>"+item.device.brand+"-"+item.device.model+"</td></tr>";
  tabla=tabla + "<tr><td>S.O.</td><td>"+item.device.operativeSystem+" "+item.device.osVersion+"</td></tr>";
  tabla= tabla + "</tbody></table>";
  return tabla;
}

function filterAlerts(map,startDate,endDate,clearAll){
    if(!clearAll){
      var f = new Date(startDate.val());
      var t = new Date(endDate.val());
      if(t<f){
        alert("La fecha final no puede ser menor a la inicial");
        endDate.val("");
        return;
      }
      var f = convertUTCDateToLocalDate(new Date(startDate.val()));
      var t = convertUTCDateToLocalDate(new Date(endDate.val()));
      f.setHours(0,0,0,0);
      t.setHours(23,59,59,999);
    }

    alerts.clearLayers();  
    
    $.ajax({
    type: "GET",
    url: "https://ruidox-demo.cfapps.io/measurement/",
    dataType:"json"
    }).done(function(data) {
      $.each(data, function(i, item) {
        color = getColorByLevel(item.measurement);
        marker=L.marker([item.latitude, item.longitude], {icon: L.AwesomeMarkers.icon({
          icon: 'volume-up', 
          prefix: 'glyphicon', 
          markerColor: color}) 
        }).bindPopup(setTable(item));
        if(item.measurementDate != null){
          var date = new Date(item.measurementDate.toString().substring(0, 23));
          if(clearAll){
            alerts.addLayer(marker);
          }
          else if(date>=f && date<=t){
              alerts.addLayer(marker);
          }
        }
      });
    });
}

function getLegend(){
    strCode = "<tr><td bgcolor='#D63E2A' style='color: white'><i class='glyphicon glyphicon-volume-up  icon-white'></i></td><td>Ruido extremo</td></tr>";
    strCode = strCode + "<tr><td bgcolor='#F69730' style='color: white'><i class='glyphicon glyphicon-volume-up  icon-white'></i></td><td>Daño auditivo</td></tr>";
    strCode = strCode + "<tr><td bgcolor='#FFCA90' style='color: white'><i class='glyphicon glyphicon-volume-up  icon-white'></i></td><td>Ruido no peligroso</td></tr>";
    strCode = strCode + "<tr><td bgcolor='#BBF970' style='color: white'><i class='glyphicon glyphicon-volume-up  icon-white'></i></td><td>Confortable</td></tr>";
    strCode = strCode + "<tr><td bgcolor='#38A9DC' style='color: white'><i class='glyphicon glyphicon-volume-up  icon-white'></i></td><td>Imperceptible</td></tr>";
    return strCode;
}

function convertUTCDateToLocalDate(date) {
    var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);
    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();
    newDate.setHours(hours - offset);
    return newDate;   
}